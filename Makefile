.PHONY: all build test docker-image

default: all

all: build

build:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/helloServe main.go

test: 
	go test -v ./... -cover

docker-image:
	docker build -t helloserve:latest -f build/Dockerfile .