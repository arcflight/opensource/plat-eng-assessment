package main

import (
	"net/http"

	"gitlab.com/arcflight/opensource/plat-eng-assessment/pkg/hello"
)

func main() {
	http.HandleFunc("/hello/", hello.HttpHello)

	http.ListenAndServe(":8080", nil)
}
