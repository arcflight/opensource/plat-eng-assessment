/*
Copyright © 2021 TrustFlight Ltd <infrastructure@trustflight.io>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package hello

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_httpHello(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"default",
			args{},
			"Hello, World!",
		},
		{
			"default",
			args{
				"TrustFlight",
			},
			"Hello, TrustFlight!",
		},
	}
	testHandler := http.HandlerFunc(HttpHello)
	testServer := httptest.NewServer(testHandler)
	defer testServer.Close()
	client := testServer.Client()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res, err := client.Get(testServer.URL + "/hello/" + tt.args.name)
			if err != nil {
				t.Error(err)
			}
			defer res.Body.Close()

			body, err := ioutil.ReadAll(res.Body)
			if err != nil {
				t.Error(err)
			}

			if string(body) != tt.want {
				t.Fatalf("Wanted %v, got %v", tt.want, string(body))
			}
		})
	}
}
