/*
Copyright © 2021 TrustFlight Ltd <infrastructure@trustflight.io>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package hello

import (
	"fmt"
	"net/http"
	"strings"
)

func HttpHello(w http.ResponseWriter, req *http.Request) {
	name := strings.TrimPrefix(req.URL.Path, "/hello/")
	if name != req.URL.Path && name != "" {
		fmt.Fprintf(w, "Hello, %v!", name)
	} else {
		fmt.Fprintf(w, "Hello, World!")
	}
}
